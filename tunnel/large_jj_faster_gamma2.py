from int import print_parameters

# JJ parameters from Chen 2011
print_parameters(C = 45e-12, # Farad
				 I0 = 210e-6, # Ampere
				 beta = 0.99825)
