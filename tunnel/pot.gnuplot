w(phi) = -cos(phi) - beta*phi # phi = 2πФ/Ф0

wmin(beta) = -beta*asin(beta) - sqrt(1-beta**2)
phimin(beta) = asin(beta)

deltamax(beta) = 2. * sqrt(1-beta**2) / beta
wmax(beta) = 2. * (1-beta**2)**1.5 / beta**2 / 3

wappr(delta, beta) = sqrt(1-beta**2)/2*delta**2 - beta/6*delta**3 # delta = phi-phimin

set xlabel "(Phi-Phimin)/Phi0"
set ylabel "E/Wj"

set samples 256
beta=0.97987; plot [-0.1:0.15][-.6*wmax(beta) : 1.6*wmax(beta)]\
		w(2*pi*x + phimin(beta)) - wmin(beta) t"Exact", \
		wappr(2*pi*x, beta) t"Cubic approximation"
