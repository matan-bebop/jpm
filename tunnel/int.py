# vi: ts=4
# vim: set expandtab

from scipy import integrate, sqrt, cos, arcsin, arccos, exp, pi
from numpy import linspace
from scipy.special import gamma
import sys

e = 1.6e-19 # C
Phi0 = 2.06e-15 # Wb
hbar = 1.05e-34 # m²kg/s

phimin = lambda beta: arcsin(beta)

delta0 = lambda beta: 3*sqrt(1-beta**2)/beta

wmin = lambda beta: -beta*arcsin(beta) - sqrt(1-beta**2) 

wmax = lambda beta: wmin(beta) + 2 * (1-beta**2)**1.5 / beta**2 / 3


def w(phi, beta):
    """ Washboard potential in the units of Wj with tilt set by beta=I/I0 """
    return -beta*phi - cos(phi)


def ep(beta, WcWj_ratio):
    """ Plasma energy: the energy difference between the first exited and the
        ground level E1-E0 in the harmonic oscillator approximation, in the 
        units of Wj """
    return sqrt(8*WcWj_ratio)*(1-beta**2)**.25


def w0(beta): 
    """ Potential barrier height in the units of Wj """
    return wmax(beta) - wmin(beta)


def n0(beta, WcWj_ratio):
    """ How much plasma frequencies ep fits the pot """ 
    return w0(beta) / ep(beta, WcWj_ratio)


def phi_turn(k, eps, beta):
    """ kth turning point of a particle with energy eps in the cubic 
        approximation. eps is in the units of Wj. """
    cosφmin = sqrt(1 - beta**2)
    ctgφmin = cosφmin / beta
    theta_k = arccos(1 - 3*eps/ctgφmin**2/cosφmin) + 2*pi*k
    return (1 + 2*cos(theta_k/3)) * ctgφmin 


def DapprStrauch(eps, beta, WcWj_ratio):
    """ Approximate tunneling exponent as given by Strauch. eps is the energy
        of the state in the units of Wj. """
    E = (eps-wmin(beta)) / ep(beta, WcWj_ratio) # energy in the W1-W0 units
    Ns = w0(beta)/ep(beta, WcWj_ratio)
    # S1 + E/2 log(E/432/Ns) = S from Strauch's thesis, p. 185
    S1 = 18*Ns/5 - E/2
    return exp(-2*S1)*(432*Ns/E)**E
    #S1 = 27*Ns/30 - E/2
    #return exp(-2*S1)*(108*Ns/E)**E


def D(eps, beta, WcWj_ratio):
    """ Tunneling exponent of a state. eps is its energy in the units of Wj. """
    phi1 = phimin(beta); phi2 = phi1 + delta0(beta)

    cut = lambda x: x if x>0 else 0
    quad = integrate.quad(lambda phi: sqrt(cut(w(phi, beta) - eps)),
                          phi1, phi2)[0]

    return exp(-quad/sqrt(WcWj_ratio))


def Dhi(eps, beta, WcWj_ratio):
    """ Approximate tunneling exponent for high energies. eps is the energy
        of the state in the units of Wj. """ 
    return exp(pi/sqrt(2*WcWj_ratio)
              * (eps-wmin(beta) - w0(beta)) * (1-beta**2)**-.25)


def Dveryhi(eps, beta, WcWj_ratio):
    return 1/(1 + exp(-pi/sqrt(2*WcWj_ratio) * (eps-wmin(beta) - w0(beta))
                      * (1-beta**2)**-.25))

def top_overestimate(n, beta, WcWj_ratio):
    """ Estimates how the usual WKB overestimates tunneling rate close to the
        top of the barrier """
    d = D(wmin(beta)+ep(beta,Wc/Wj)*lappr(n, beta, Wc/Wj), beta, Wc/Wj)
    dtop = Dveryhi(wmin(beta)+ep(beta,Wc/Wj)*lappr(n, beta, Wc/Wj), beta, Wc/Wj)
    return (d - dtop)/d


def lappr(n, beta, WcWj_ratio):
    """ nth state energy in a cubic approximation in the units of E1-E0."""
    A = 5. / n0(beta, WcWj_ratio) / 72
    return .5 + n - A*(n**2 + n + 11./30)


def levels(beta, WcWj_ratio, E):
    """ List of energy levels as given by function E """
    cn0 = n0(beta, WcWj_ratio)
    n=0; Es=[]
    Es.append(E(n))
    while True:
        if E(n) > cn0:
            return Es
        Es.append(E(n))
        n = n+1


# n0 such as the 2nd excited level of a 3-level pot is as far from the top
# as it is possible. The formula used is correct in the cubic approximation
# when the unharmonicity is small.
n0far = (756 + sqrt(756**2 - 432*371))/432


def beta_for_n0(n0_given, WcWj_ratio):
    """ beta such for a given n0 and Wc/Wj """
    from scipy.optimize import newton
    return newton(lambda beta: n0(beta, WcWj_ratio) - n0_given, 0.99)


def g(n, beta, Wc, Wj, d=D):
    """ Gives the tunneling rate from the level of energy n. Energy is in the
        units of E1-E0. """
    C = Wj*ep(beta, Wc/Wj) * ((n + 0.5)/exp(1))**(n + 0.5) \
        / sqrt(2*pi) / hbar / gamma(n+1)
    return C*d(wmin(beta) + (n+0.5)*ep(beta, Wc/Wj), beta, Wc/Wj)


def print_parameters(C, I0, beta):
    """ Prints out C, I0, beta, and the respective tunneling rates, plasma 
        frequency Wp, transition frequencies W10 and W20, and the detuning Δ"""
    print("C =", C*1e12, "pF,",
          "I0 =", I0*1e6, "μA,",
          "I/I0 =", beta)

    Wc = e**2 / 2/C
    Wj = I0*Phi0 / 2 / pi

    print("Wj/Wc =", Wj/Wc)

    g2 = g(2, beta, Wc, Wj)
    assert(abs(g2 - g(2, beta, Wc, Wj, Dhi))/g2 < 0.1)
    print("γ2 = 2π", 1e-6 * g2 /2/pi, "MHz", end=", ")

    g1 = g(1, beta, Wc, Wj)
    assert(abs(g1 - g(1, beta, Wc, Wj, DapprStrauch))/g1 < 0.2)
    print("γ1 = 2π", 1e-3 * g1 /2/pi, "KHz", end=", ")

    g0 = g(0, beta, Wc, Wj)
    assert(abs(g0 - g(0, beta, Wc, Wj, DapprStrauch))/g0 < 0.1)
    print("γ0 = 2π", g0 /2/pi, "Hz")

    print("Wp = 2π", 1e-9*ep(beta,Wc/Wj)*Wj/hbar/2/pi, "GHz")

    n10 = lappr(1, beta, Wc/Wj) - lappr(0, beta, Wc/Wj)
    W10 = ep(beta, Wc/Wj)*n10 * Wj/hbar # Hz
    print("W10 = 2π", 1e-9*W10/2/pi, "GHz", end=", ")

    n20 = lappr(2, beta, Wc/Wj) - lappr(0, beta, Wc/Wj)
    W20 = ep(beta, Wc/Wj)*n20 * Wj/hbar # Hz
    print("W20 = 2π", 1e-9*W20/2/pi, "GHz")

    Delta = ep(beta, Wc/Wj)*(n10-n20/2) * Wj/hbar # Hz
    # Double check: the 5/72 formula can be obtained easily
    assert(Delta - ep(beta, Wc/Wj)*5/72/n0(beta,Wc/Wj) * Wj/hbar < 1e-5)
    print("Δ = W10 - W20/2 = 2π", 1e-6 * Delta/2/pi, "MHz") 


def plot_Ds_and_levels(C, I0, beta, filename=''):
    """ Plots D, Dhi, Dveryhi, DStrauchs alongside with the energy levels.
        If savetxt is true, saves Ds to a file """
    Wc = e**2 / 2/C
    Wj = I0*Phi0 / 2 / pi

    print("I/I0 =", beta)
    print("Wj/Wc =", Wj/Wc)

    epss = linspace(wmin(beta), wmax(beta), 100)
    epssall = linspace(wmin(beta), wmax(beta) + 0.5*w0(beta), 150)
    Ds = [D(eps, beta, Wc/Wj) for eps in epss]
    DStrauchs =  [DapprStrauch(eps, beta, Wc/Wj) for eps in epss[1:]]
    Dhis =  [Dhi(eps, beta, Wc/Wj) for eps in epss]
    Dveryhis = [Dveryhi(eps, beta, Wc/Wj) for eps in epssall]

    if len(filename) == 0:
        import matplotlib.pyplot as plt

        epss = (epss - wmin(beta))/ep(beta, Wc/Wj)
        epssall = (epssall - wmin(beta))/ep(beta, Wc/Wj)
        plt.plot(epss, Ds, label='Numerical integration')
        plt.plot(epss[1:], DStrauchs, label='Strauch approximation')
        plt.plot(epss, Dhis, label='High-energy approximation')
        plt.plot(epssall, Dveryhis, label='Near-the-top approximation')
       
        for l in levels(beta, Wc/Wj, lambda n: lappr(n, beta, Wc/Wj)):
            plt.axvline(x=l, ls='dashed')

        plt.axvline(x=n0(beta, Wc/Wj), color='black')
        plt.axvline(x=n0(beta, Wc/Wj)-1, color='gray', ls='dashed')
        
        plt.legend()
        plt.show()
    else:
        from numpy import savetxt, column_stack
        rel_epss = [eps - wmin(beta) for eps in epss]
        savetxt(filename, column_stack((rel_epss, Ds)))


# vim: set ts=4 sw=4 expandtab
