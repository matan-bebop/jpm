# Please run with python3 -O to disable assertions

from int import *
from math import ceil

C = 0.2e-12 # Farad
I0 = 1e-6 # Ampere

Wc = e**2 / 2/C
Wj = I0*Phi0 / 2 / pi

beta = 1e-5*ceil(1e5*beta_for_n0(3.13, Wc/Wj))

# Here an assertion is raised: Strauch approximation no longer works due to
# the breakdown of the cubic approximation
print_parameters(C, I0, beta)

print("W3 - Wmax = 2π", (lappr(3, beta, Wc/Wj)*ep(beta,Wc/Wj)
                      - (wmax(beta)-wmin(beta)))*Wj/hbar/2/pi * 1e-6,
      "MHz")
