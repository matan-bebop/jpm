from math import ceil
from int import *

C = 2e-12 # Farad
I0 = 10e-6 # Ampere

Wc = e**2 / 2/C
Wj = I0*Phi0 / 2 / pi

beta = 1e-5*ceil(1e5*beta_for_n0(2.2, Wc/Wj))

g1 = g(1, beta, Wc, Wj)

print("I/I0 = ", beta)
print("γ1 = 2π", 1e-6 * g1 /2/pi, "MHz")
