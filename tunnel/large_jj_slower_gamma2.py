from int import *
from math import ceil

# JJ parameters from Chen 2011
C = 45e-12 # Farad
I0 = 210e-6 # Ampere

Wc = e**2 / 2/C
Wj = I0*Phi0 / 2 / pi

beta = 1e-5*ceil(1e5*beta_for_n0(n0far, Wc/Wj))

print_parameters(C, I0, beta)

print("W3 - Wmax = 2π", (lappr(3, beta, Wc/Wj)*ep(beta,Wc/Wj)
                      - (wmax(beta)-wmin(beta)))*Wj/hbar/2/pi * 1e-6,
      "MHz")
