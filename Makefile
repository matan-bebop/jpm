paper.pdf: paper.tex Lcorij.tex Makefile fizychni_komandy.tex notations.tex levels/cartoon.pdf levels/rates.pdf circuit/jpmres.pdf circuit/altjpm.pdf pot.pdf altpot.pdf probability2photon/err.pdf
	pdflatex paper
	bibtex paper
	pdflatex paper
	pdflatex paper

pot.pdf: pot.gnuplot
	gnuplot pot.gnuplot
	pdflatex pot.tex

altpot.pdf: altpot.gnuplot
	gnuplot altpot.gnuplot
	pdflatex altpot.tex

probability2photon/err.pdf:
	$(MAKE) -C probability2photon

Lcorij.tex: lindblad.mac qsimp-jpm.mac
	maxima < lindblad.mac

extra.tar.bz2: $(wildcard extra/*)
	tar -cvhf - -C extra . | bzip2 - > extra.tar.bz2

upload-extra: extra.tar.bz2
	dropbox-uploader upload extra.tar.bz2 jpm-misc/

upload-paper: paper.pdf
	dropbox-uploader upload paper.pdf jpm-misc/

upload: upload-paper upload-extra

.PHONY: probability2photon/err.pdf

