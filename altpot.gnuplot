set term epslatex color size 2.4cm,2.15cm standalone colortext
set out 'altpot.tex'
w = 2.4 #cm
h = 2.15 #cm

set border 0
unset tics
set margins 2, 0, .5, at screen 0.9

w(phi) = -cos(phi) + beta*(phi-phie)**2 # phi = 2πФ/Ф0
beta = 0.12
phie = -1
phi_from = -2.2
phi_to = 1.8
w_graph = (phi_to - phi_from)*pi
h_graph = abs(w(2*pi))

darkgray="#ff666666"
set arrow from screen 0.1,0.1 to screen 0.3,0.1 lc rgb darkgray filled
set arrow from screen 0.1,0.1 to screen 0.1,0.3 lc rgb darkgray filled
set label '$\Phi$' at screen 0.3,0.1 textcolor rgb darkgray
set label '$W$' at screen 0.1,0.3 textcolor rgb darkgray
set label '\textbf{(b)}' at screen 0,1 offset character 0.3,-0.5 \
			textcolor rgb "black"

solid=1; set style line solid lc rgb "black" dt solid lw 2

plot [phi_from*pi:phi_to*pi][:w(phi_to*pi)] w(x) ls solid t''
