Superconducting Detector That Counts Microwave Photons Up to Two
================================================================

Project codes and TeX sources used to produce the Phys. Rev. Applied 14, 064063 
paper by A. M. Sokolov and F. K. Wilhelm.

The detector is a Josephson photomultiplier (JPM) that uses a two-photon
transition "jumping over" a middle level in the JPM metastable well. It allows
the JPM to absorb photon pairs. After a check for pairs, the JPM can be rapidly
tuned into the regular single-photon regime. Hence in two steps the JPM counts
to two, i.e. gives three possible outcomes "no input photons", "a single one",
and "two photons or more".

Here we provide the repository overview.


Scripts
-------

Makefile generates the full `paper.pdf`. Use it by simply running `make` in the
repository folder. Requires some .bib files from my
https://gitlab.com/matan-bebop/bib repository to be seen by LaTeX. Besides
the LaTeX suite, requires Gnuplot and Maxima.


There are some Maxima (https://maxima.sourceforge.io) codes that we
used to produce lengthy analytical results.

`lindblad.mac` calculates the expressions for the Lindbladian matrix
elements in our working frame and outputs it as LaTeX. It uses:

`qsimp-jpm.mac` which is a rather limited program that allows to
simplify expressions with bras and kets of a JPM and the creation and
annihilation operators of a resonator mode.

In the `master` branch, we use a tuneling model with a Lindbladian
mostly the same as for the dissipation. For the Ping-Li-Gurvitz
tunneling model, switch to the `gurvitz` branch.

`tunnel/` 
Calculation of the tunneling rates in the washboard potential. The main
codes are in `int.py`. Other python files contain parameters of various
Josephson junctions. `pot.gnuplot` plots the JPM potential for the parameters
used in the paper, as well as its cubic approximation.

`counting_error.py`
Calculates the full counting error.


Figures
-------

There are Gnuplot (http://www.gnuplot.info) scripts to generate figures and
some illustrations in pdf. All pdfs in the repository are created with Ipe
(https://ipe.otfried.org).

`pot.gnuplot`
Plot the washboard potential for some illustrative parameters

`altpot.gnuplot`
Potential of an alternative JPM---an rf-SQUID with two potential minima

`circuit/`
circuit diagrams from the paper

`levels/`
JPM level structure and its working principle (cartoon.pdf), JPM-resonator
level structure and the processes in the fast tunneling limit (rates.pdf)

`probability2photon/`
Calculation and plotting of the count probabilities in the two-photon regime.
`err.gnuplot` plots the probability to detect a two-photon state with time, as
well as the error in the detection of a multiphoton state in the inset.
`dark.gnuplot` plots the ratio of the error due to undetected two-photon state
and the error due to the false counts for two parameter sets. Several parameter
sets based on the [Martinis 2005] paper are stored in the `martinis_*.inc`
files. Tunneling rates in these files were obtained using the `tunneling/int.py`
routines and the parameter Python files stored in the `tunneling/` folder.

`probability2photon/dephased/`
Scripts to plot the two-photon detection probability for the various regimes of
the JPM in the artificially dephased regime, where its pure dephasing dominates.

Texts
-----

The `.tex` files in the root directory contain the paper text and the
various LaTeX commands that we use.

`letter.txt` is the cover letter we attached to the paper submission.
