#!/usr/bin/python3

Pbr1 = 19/(19+0.318) # single-photon detection bright count probability
Pbr2 = 0.985 # two-photon detection bright count probability
Pf = 0.7e-2 # false count probability in the two-photon detection
eps = (Pf*(2+Pbr1) + 1-Pbr2 + 1-Pbr1 - Pf**2)/3

print('Counting error is', eps*100, '%.')
