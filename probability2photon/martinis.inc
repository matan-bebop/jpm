# Frequencies in MHz, time in µs

print "Assigning numerical values: JJ from a hi-coherence phase qubit\
	[Martinis 2005], n0 that gives a well with fast g2"
g0 = 2*pi*37e-6
g1 = 2*pi*54e-3
g2 = 2*pi*41

g1_1 = 2*pi*19 # g1 in the two-level configuration

T10 = 500e-3 #µs
T2 = 150e-3 #µs
G10 = 1/T10
G21 = 2*G10
G22 = 2/T2 # We have exp(-G22 t/2) by definition

l2 = 0.1
Delta = 2*pi*194.7
G22full = g2 + G21 + G22 # Coherence limited by tunneling and relaxation
print "g2=2π",g2/2/pi, " MHz, G21=2π",G21/2/pi, " MHz, G22=2π",G22/2/pi, " MHz"
G20 = 4 * l2**4 * Delta**2 / G22full
print "G20=2π", G20/2/pi, " MHz"

