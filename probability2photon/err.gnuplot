if(!exists('interactive') || !interactive) {
	set term epslatex size 8.6cm,6.3cm monochrome standalone \
					  header '\include{notations}'	
	set out 'err.tex'

	interactive = 0
} else {
	set term x11
}

set xlabel '$t$ ($\mu$s)'

unset title
unset key

load 'martinis.inc'
load 'formulas.inc'

set multiplot

# Plot the probability of missing the two-photon state
x1 = 0.5; x2 = 5.
y1 = 1; y2 = 30.
set ytics add (y1, y2)
set logscale y
set ylabel '$1-\Pbright$ (\%)'
plot [x1:x2][y1:y2] 100*(1-Ptun(x))

# Plot the error probability in the inset
set origin 0.35, 0.35
set size 0.6, 0.55
set ytics add (y1)
set logscale
x1 = 0.1; x2 = 500.
y1 = 0.5; y2 = 100.
set ylabel '$\varepsilon$ (\%)' offset 1
set xlabel offset 0,0.5
plot [x1:x2][y1:y2] 100*0.5*(1-Ptun(x)+Pf(x))

unset multiplot
reset

topt = log(G20/g0)/G20
print ""
print "topt = ", topt, " us"
print "Pf(topt) = ", Pf(topt)*100, "%"
print "Pbr(topt) = ", Ptun(topt)*100, "%"

print ""
Pbr1 = g1_1/(g1_1+G10)
eps_full = (Pf(topt)*(1+Pbr1) + 1-Pbr1 + 1-Ptun(topt))/3
print "eps_full = ", eps_full*100, "%"
