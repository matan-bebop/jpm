if(!exists('interactive') || !interactive) {
	set term epslatex size 12cm,9cm standalone \
			 header '\usepackage{arev}' font 20
	set out 'dark.tex'

	borderstyle=50; set style line borderstyle lt 1 lc rgb "black" lw 5
	set border ls borderstyle

	interactive = 0
} else {
	set term x11
}

load 'colors.inc'

set rmargin 0.6
set lmargin 6
set tmargin 2
set bmargin 3

set ylabel '$(1-P_\mathrm{br})/P_\mathrm{d}$, \%'
set xlabel '$t$, $\mu$s'
set tics; set border 15

set logscale
unset title
unset key

set multiplot

load 'martinis.inc'
load 'formulas.inc'
plot (1-Ptun(x))/Pf(x) lw 8 lc rgb color_large_jj

unset tics; unset xlabel; unset ylabel; set border 0

load 'martinis_tiny_jj.inc'
load 'formulas.inc'
plot (1-Ptun(x))/Pf(x) lw 8 lc rgb color_small_jj

unset multiplot
set tics; set xlabel; set ylabel; set border 15
