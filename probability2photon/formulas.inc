G2 = G21 + g2
G1 = G10 + g1

# Decoherence dominated by tunneling and relaxation

# especially inaccurate for t < 1/g0
Ptun(t) = 1 - exp(-G20*t) - G21*G10/G1/g2*exp(-g0*t)

# Dark count probability

Pf(t) = 1 - exp(-g0*t)
