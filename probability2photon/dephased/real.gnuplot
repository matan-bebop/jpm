if(!exists('interactive')) {
	set term epslatex size 8.6cm,6.3cm standalone monochrome
	set out 'real.tex'
} else {
	set term x11
}

set rmargin 0.6
set lmargin 6
set tmargin 2
set bmargin 3

set ylabel '$1-P$, \%'
set xlabel '$t$, $\mu$s'
set tics; set border 15

set logscale y
unset title
unset key

range = 0.6 # Vary the tunneling rate in [g2 - range/2, g2 + range/2]
steps = 4 # in a certain number of steps

load 'real.inc'
load 'formulas.inc'
g2_0 = g2
start = g2_0*(1-0.5*range)
end = g2_0*(1+0.5*range)
increment = g2_0*range/steps

set multiplot

x1 = 1.; x2 = 5
y1 = 3.; y2 = 20.

set ytics add (y1, y2)

g2 = start
load 'real.inc'
load 'formulas.inc'
plot [x1:x2][y1:y2] 100*(1-P(x))#, 100*(1-Pappr(x))

unset tics; unset xlabel; unset ylabel; set border 0
do for \
 [g2 = start + increment : end : increment] {
	load 'real.inc'
	load 'formulas.inc'
	plot [x1:x2][y1:y2] 100*(1-P(x))#, 100*(1-Pappr(x))
}

g2 = g2_0

unset multiplot
