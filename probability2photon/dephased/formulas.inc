G2 = G21 + g2
G1 = G10 + g1

# Fast decoherence

s(sign) = 0.5 * (-2*G20 - G2 - g0 + sign*sqrt(4*G20**2 + (G2-g0)**2))
sp = s(+1)
sm = s(-1)

A(s,t) = (g0*G20*G21*G10/s/(s+g0)/(s+G1) + g1*G20*G21/s/(s+G1) \
	+ (G20*(g0+g2) + g0*G2)/s + g0) * exp(s*t)

P(t) = 1 - G21*G10/(G1-g0)/(G2-g0)*exp(-g0*t) \
	 + (g1-g0)*G20*G21*exp(-G1*t)/(G1+sp)/(G1+sm)/(g0-G1) \
	 + (A(sp,t) - A(sm,t))/(sp-sm)

# Decoherence dominated by tunneling and relaxation

# especially inaccurate for t < 1/g0
Ptun(t) = 1 - exp(-G20*t) - G21*G10/G1/g2*exp(-g0*t)
