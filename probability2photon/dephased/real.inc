# Frequencies in MHz, time in mks

# Don't reassign values if loaded second time. Thus you can change smth and
# reload to apply changes to Pappr.
if(!exists("real_inc_loaded") || real_inc_loaded == 0) {
	print "Assigning numerical values."
	g0 = 2*pi*12e-6
	g1 = 2*pi*18e-3
	g2 = 2*pi*14

	G10 = 2*pi*300e-3
	G21 = 2*G10

	l2 = 0.1
	Delta = 2*pi*102.4
	G22 = g2 + G21 # Coherence limited by tunneling and relaxation
	G20 = 4 * l2**4 * Delta**2 / G22
	print G20
}

sappr(sign) = 0.5 * (-2*G20 - g2 + sign*sqrt(4*G20**2 + g2**2))
spappr = sappr(+1)
smappr = sappr(-1)

Pappr(t) = 1. + g2*G20*exp(spappr*t)/spappr/(spappr - smappr) \
			 - g2*G20*exp(smappr*t)/smappr/(spappr - smappr)

real_inc_loaded = 1
