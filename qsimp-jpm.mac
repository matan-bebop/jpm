/* qsimp-jpm.mac

A primitive simplifier for Dirac bra and ket notation, Hermitian conjugate and
an operator 'a' that does not act on bras and kets. Made for calulations for the
system of a Josephson photon multiplier (JPM) and a resonator.

Author: Andrii M. Sokolov

Provides:
- a syntax to write out bras, kets, and Hermitian conjugate. Quantum state in
  bras and kets can only be denoted by an integer or 'm'.
- qsimp(expr) a function to simplify expressions with bras and kets, operator
  'a' that commutes with bras and kets, and Hermitian conjugates of those.
- tex(...) prettyprint.

Example usage:

(%i1) declare(a, nonscalar)$
(%i2) qsimp(bra(0).ket(1));
(%o2)                                 0
(%i3) qsimp(bra(0).a.ket(0));
(%o3)                                 a
(%i4) qsimp(bra(m).(a~.a.bra(m)~));
(%o4)                              a ~ . a

Bugs:
- Raising bras, kets, and operators to a power is not allowed

*/


declare([bra, ket], nonscalar)$

/* The inner product rule */
matchdeclare([ii, jj], lambda([t], featurep(t, integer)))$
tellsimp(bra(ii).ket(jj), kron_delta(ii,jj))$
tellsimp(bra(ii).ket(m), 0)$
tellsimp(bra(m).ket(jj), 0)$
tellsimp(bra(m).ket(m), 1)$

/* Tell Maxima that */
dotscrules:true$ /* a.(b.scalar) = scalar * a.b */
dotdistrib:true$ /* a.(b+c) = a.b + a.c */

/* Handling of (...)^^2 is not implemented in the code below. Hence we prevent
 * Maxima to simplify A.A to A^^2 */
dotexptsimp:false$

matchdeclare([aa,bb,cc], nonscalarp)$
matchdeclare([ll,dd], all)$

block([simp:false],
	defrule(shuffle, aa.(bb.cc), (aa.bb).cc),

	defrule(comma1, bra(ll).a, a.bra(ll)),
	defrule(comma2, ket(ll).a, a.ket(ll)),
	defrule(comm3a1, bra(ll).(a.dd), a.(bra(ll).dd)),
	defrule(comm3a2, ket(ll).(a.dd), a.(ket(ll).dd))
)$

/* Let a~ denote the Hermitian conjugate of a */
/* TODO: Use the built-in conjugate()? */
postfix("~")$

declare("~", [additive, multiplicative])$
matchdeclare(rr, lambda([t], featurep(t, real)))$
matchdeclare(ii, lambda([t], featurep(t, imaginary)))$
defrule(herre, rr~, rr)$
defrule(herim, ii~, -ii)$
defrule(hermulr, (ll.dd)~, dd~.ll~)$
defrule(hermull, (dd.ll)~, ll~.dd~)$
defrule(herher, dd~~, dd)$
defrule(herpow, (dd^ll)~, dd~^ll~)$

defrule(braher, bra(ll)~, ket(ll))$
defrule(kether, ket(ll)~, bra(ll))$

block([simp:false],
	defrule(commadag1, bra(ll).a~, a~.bra(ll)),
	defrule(commadag2, ket(ll).a~, a~.ket(ll)),
	defrule(comm3adag1, bra(ll).(a~.dd), a~.(bra(ll).dd)),
	defrule(comm3adag2, ket(ll).(a~.dd), a~.(ket(ll).dd))
)$

/* These definitions differ in sign with those used in the paper, the thesis,
   and the handwritten notes! */
sigmaz01 : ket(0).bra(0) - ket(1).bra(1)$
sigmaz12 : ket(1).bra(1) - ket(2).bra(2)$

/*
 * Converts an expression e to the right-associative form 
 */
torassocform(e) := block(
	ev(e, dotassoc=true),
	ev(%%, dotassoc=false)
)$

/*
 * Moves a and a~ as left as it is possible. Takes an expression e in the right-
 * associative form; the result is given in that form as well.
 */
commasimp(e) := block(
	applyb1(e, comma1, comma2, comm3a1, comm3a2),
	applyb1(%%, commadag1, commadag2, comm3adag1, comm3adag2),

	if(not(e = %%))	then
		commasimp(%%) /* Repeat until e ceases to change */
	else
		%%
)$

/*
 * Simplifies what concerns ~. Takes an expression e in the right-
 * associative form; the result is given in that form as well.
 */
hersimp(e) := block(
	apply2(e, herpow, hermulr, hermull, herher, herim, herre, braher, kether),

	if(not(e = %%))	then
		hersimp(%%) /* Repeat until e ceases to change */
	else
		%%
)$


/*
 * Simplifies what concerns bras, kets, a, and ~ in e. Returns an expression in
 * a normal flattened form like a.b.c.d
 */
qsimp(e) := block(
	[dotassoc : false], 
	torassocform(e),
	hersimp(%%),
	torassocform(%%),
	commasimp(%%),
	apply2(%%, shuffle), /* on the way from the r-assoc to the l-assoc form,
	* each two neighbours o1 and o2 are eventually multiplied, (o1.o2). As 
	* apply2 simplifies the full glued expression after a part matches a rule, 
	* products like bra(1).ket(1) simplify with the tellsimp rules provided. */
	ev(%%, dotassoc=true)
)$

/* TeX prettyprint*/

texbraket : true$ /* requires \bra and \ket defined if true */

tex_state_label(l) := if is(l=m) then "\\mathrm m" else tex1(l)$

/* For compatibility with breqn, \Bra and \Ket are supposed to use 
   \left and \right */

texbra(op) := 
	if texbraket then concat("\\Bra{", tex_state_label(op), "}")
				 else concat("\\langle ", tex1(op), "|")$
texket(op) := 
	if texbraket then concat("\\Ket{", tex_state_label(op), "}")
				 else concat("|", tex1(op), "\\rangle")$

texput(bra, lambda([l], texbra(args(l)[1])))$
texput(ket, lambda([l], texket(args(l)[1])))$

texput(".", " ", nary)$

texput("~", "^\\dag", postfix)$

