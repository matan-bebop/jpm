set term epslatex color size 1.8cm,3.5cm standalone colortext
set out 'pot.tex'
w = 1.8 #cm
h = 3.5 #cm

set border 0
unset tics
set margins 2, 0, .5, at screen 0.9

w(phi) = -cos(phi) - beta*phi # phi = 2πФ/Ф0
beta=0.8
w_graph = 8*pi
h_graph = abs(w(w_graph))

darkgray="#ff666666"
lx = 0.4
ly = lx*w/w_graph * h_graph/h
set arrow from graph 0,0 to graph lx, 0 lc rgb darkgray filled
set arrow from graph 0,0 to graph 0, ly lc rgb darkgray filled
set label '$\Phi$' at graph lx, 0 textcolor rgb darkgray
set label '$W$' at graph 0, ly textcolor rgb darkgray
set label '\textbf{(b)}' at screen 0,1 offset character 1,-0.5 \
						textcolor rgb "black"

solid=1; set style line solid lc rgb "black" dt solid lw 2
dashed=2; set style line dashed lc rgb "black" dt '-' lw 2

theta(x) = x>0? 1:1/0

plot [0:8*pi][w(8*pi):w(0)] theta(x-pi)*theta(1.5*pi-x)*w(x) ls dashed t'',\
	theta(x-1.5*pi)*theta(7.5*pi-x)*w(x) ls solid t'',\
	theta(x-7.5*pi)*w(x) ls dashed t''
